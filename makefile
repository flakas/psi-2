pdf : build_folder
	pandoc --toc --latex-engine=xelatex -o build/projektas.pdf -N --template=template.tex projektas/*.md

html : build_folder
	pandoc --toc --latex-engine=xelatex -o build/projektas.html -N projektas/*.md

getimg :
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-63F5A.png' -O 'projektas/img/agentu_hierarchija.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-FA416.png' -O 'projektas/img/nepris_uzduociu_diagrama.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-41ADB.png' -O 'projektas/img/uzduociu_diagrama.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-6A525.png' -O 'projektas/img/kompozicija_komponentai.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-1DB57.png' -O 'projektas/img/kompozicija_diegimas.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-FE0CD.png' -O 'projektas/img/logine_marsrutizatorius.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-99F17.png' -O 'projektas/img/logine_skaitykle.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-56B36.png' -O 'projektas/img/logine_admin_modulis.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-CDEFC.png' -O 'projektas/img/logine_vartotojas.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-7A87C.png' -O 'projektas/img/logine_naujienu_modulis.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-55A48.png' -O 'projektas/img/logine_modeliai.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-4ACC9.png' -O 'projektas/img/saveika_prisijungti.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-CF8F1.png' -O 'projektas/img/saveika_skaityti.png'
	wget 'https://cacoo.com/diagrams/wwCiG8anzrhTMBKw-D3E0E.png' -O 'projektas/img/saveika_atnaujinti_feed.png'


resizeimg :
	find projektas/img -iname "*.png" | xargs -l -i mogrify -quality 100 -resize 75% {} {}

build_folder : clean
	mkdir build

clean :
	rm build -R
