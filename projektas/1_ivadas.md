# Anotacija

Skipbug komanda:

- Laurynas Ginkus
    * El.paštas: laurynas.ginkus@outlook.com
    * Indėlis į darbą: 20%
- Džiugas Pocius
    * El.paštas: dziugas.pocius@gmail.com
    * Indėlis į darbą: 30%
- Tautvidas Sipavičius (komandos lyderis)
    * El.paštas: tautvidas@tautvidas.com
    * Indėlis į darbą: 30%
- Martynas Šalkauskas
    * El.paštas: martynasalk@gmail.com
    * Indėlis į darbą: 20%

# Įvadas

* **Projekto pavadinimas:**  
    Automatinis aktualaus RSS turinio atrinkimas - **ARTA**

* **Dalykinė sritis:**  
    Naujienų skaitymas

    Naujienos skaitomos tiesiogiai tinklaraščiuose arba iš RSS šaltinių
    pasinaudojant RSS skaitykle. Skaityklės leidžia prenumeruoti kelis RSS
    šaltinius vienu metu, todėl patogu viską skaityti vienoje vietoje.

* **Probleminė sritis:**  
    Greitas aktualių naujienų klientui išrinkimas ir pateikimas.

* **Sistemos naudotojai:**  
    - Sistemos administratorius

        + Sprendžia iškilusias problemas, susijusias su technine sistemos dalimi
        + **Kompetencija:** aukštasis Informacinių Technologijų srities išsilavinimas

    - Klientas (Sistemos lankytojas)

        + Domisi Sistemoje pateikiamama informacija
        + **Kompetencija:** vidurinės mokyklos informatikos kursas

* **Darbo pagrindas:**  
    Dokumentas parengtas kaip Programų Sistemų Inžinerijos II-asis laboratorinis
    darbas.
