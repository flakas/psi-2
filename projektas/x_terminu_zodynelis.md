# Terminų žodynėlis

**Klientas:** Asmuo, sąveikaujantis su sistema ir tinklaraščiais, juose skaitantis naujienas. 

**Filtras:** Metodas skirtas atrinkti informaciją pagal tam tikrus kriterijus. 

**Klaidų registras:** Vieta duomenų bazėje, kurioje saugoma informacija apie įvykusias klaidas. 

**TCP/IP protokolas:** Standartinis duomenų perdavimo protokolų rinkinys, kurio pagrindu veikia internetas bei daugelis privačių komercinių tinklų.

**HTTP 1.1 protokolas:** Einamoji versija pagrindinio protokolo, kuriame pasiekia informaciją pasauliniame tinkle. Pradinė protokolo paskirtis – pateikti standartinį būdą HTML puslapių skelbimui ir skaitymui.

**Individualus serveris:** Serveris, kuris priklauso atskiram asmeniui ir jame nėra jokių svetimų sistemų.

**ATOM standartas:** Naujienų šaltiniams apibrėžti naudojamas standartas, grįstas XML kalba, kuria aprašomas tinklalapio turinys.

**HTTP portas:** Jungtis, per kurią vyksta duomenų apsikeitimas HTTP protokolu, paprastai naudojama 80 jungtis.

**Išorinio aptarnavimo (SSH) portas:** Tinklo jungtis, per kurią vyksta sistemos aptarnavimas ir priežiūra. Tai yra saugaus prisijungimo jungtis, paprastai naudojama 22 jungtis.

**Neprisijungęs klientas:** Asmuo, kuris nėra prisijungęs prie sistemos ir kurio asmenybės nustatyti negalime. Tai gali būti tiesioginis klientas ar neprisijungęs sistemos administratorius.

**Prisijungęs klientas:** : Klientas prisijungęs prie sistemos, sistema gali tiksliai atpažinti kuris asmuo tai yra.

**Prisijungęs sistemos administratorius:** Asmuo, atliekantis sistemos priežiūrą, turintis teisę prisijungti prie administratoriaus pulto.

**HTTP serveris:** Serveris, tarpininkaujantis tarp kliento ir sistemos. Perduoda kliento pateikiamus duomenis sistemai, perduoda sistemos rezultatus klientui.

**Skaityklė:**  Sistemos dalis, kurią klientas naudoja naujienų skaitymui.

**DBVS:** Duomenų bazių valdymo sistema. Tai sistemos dalis, atsakinga už sistemos duomenų saugojimą.

**Modulis:** Logiškai susijusių klasių rinkinys.

**Valdiklis:**  Klasė susiejanti modelį ir vaizdą - panaudojus modelio metodą (-us) paima duomenis iš saugyklos ir perduoda juos atitinkamai vaizdo klasei atvaizdavimui.

**Branduolys:** Pagrindinė sistemos klasė inicializuojanti būtinas sistemos darbui klases bei kintamuosius. Veikia kaip "įėjimo" į sistemą taškas.

**Maršrutizatorius:** Klasė naudojama kliento užklausai apdoroti ir reikiamos klasės ir metodo parinkimui pagal ją.

**Kroviklis:** Speciali klasė, kuri yra atsakinga už kitų klasių sukūrimą ir perdavimą į ją besikreipiančioms klasėms.

**Singleton:** Projektavimo šablonas leidžiantis sukurti tik vieną klasės objektą.

**Naujiena:** RSS šaltinyje ar tinklaraštyje publikuojamas tekstinis įrašas.  



