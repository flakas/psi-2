# Literatūros sąrašas

\begin{thebibliography}{99\kern\bibindent}

\bibitem[RS93]{RS94}
IEEE recommended practice for software requirements specifications. Prieiga per internetą: \url{http://www.math.uaa.alaska.edu/~afkjm/cs401/IEEE830.pdf}. Žiūrėta 2012-11-22.   

\bibitem[SD09]{SD09}
IEEE Standard for Information Technology - Systems Design - Software Design Descriptions. Prieiga per internetą: \url{http://dis.unal.edu.co/~icasta/GGP/_Ver_2012_1/Documentos/Normas/1016-2009.pdf}. Žiūrėta 2012-11-25.   



\end{thebibliography}
