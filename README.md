Skipbug RSS agregatorius
========================

Tema: RSS Agregatorius  
Ypatingoji funkcija: Filtravimas  

## Konsultacijų grafikas

Mūsų konsultacijos:

- Lapkričio 29 dieną - pristatom dokumentų juodraščius

## Atsiskaitymų grafikas

- Gruodžio 20 dieną 14:30 - II laboratorinio darbo pristatymas su prototipu

## Dokumento build'inimas

`make pdf` sukompiliuos PDF dokumentą (reikalingi `pandoc` ir `xelatex` paketai)  
`make html` sukompiliuos HTML dokumentą (reikalingas `pandoc` paketas)

## Dokumento struktūra

Dokumentas yra suskirstytas į sekcijas. Kiekviena mažesnė sekcija (ar didesnė subsekcija) yra suskirstyta į atskirus failus.  
Kurdami naują failą įsitikinkite, kad failus išdėsčius abėcėlės tvarka failas yra reikiamoje vietoje, nes jie bus sujungiami į vieną abėcėlės tvarka.  
Subsekcijos pratęsiamos tašku.  
Pvz.:

- `Dėstymas` pirmoji subsekcija `Verslo proceso aprašas` turės numerį 2.1

## Tutorial'ai

Markdown:

- [http://daringfireball.net/projects/markdown/syntax](http://daringfireball.net/projects/markdown/syntax)
- [http://net.tutsplus.com/tutorials/tools-and-tips/markdown-the-ins-and-outs/](http://net.tutsplus.com/tutorials/tools-and-tips/markdown-the-ins-and-outs/)

Git:

- [http://nathanj.github.com/gitguide/tour.html](http://nathanj.github.com/gitguide/tour.html)
- [http://learn.github.com/p/intro.html](http://learn.github.com/p/intro.html)

## Orientacinis dokumento turinys

* Reikalavimai sistemai
    - Reikalavimai interfeisams
        - Dalykinės srities metaforos (žodynas)
        - Formuluojamos užduotys
        - Užduočių formulavimo kalba
        - Užduočių formulavimo būdo (protokolo) reikalavimai
        - Interfeiso darnos ir standartizavimo reikalavimai
        - Pranešimų formulavimo reikalavimai
        - Interfeiso individualizuojamumo reikalavimai

    - Funkciniai reikalavimai
        + Vartotojo reikalavimai
        + Pagalbinės funkcijos

    - Nefunkciniai reikalavimai
        + Vidinių interfeisų reikalavimai
        + Veikimo reikalavimai
        + Diegimo reikalavimai
        + Aptarnavimo reikalavimai
        + Saugumo reikalavimai
        + Juridiniai reikalavimai

* Sistemos architektūra
        - Kontekstas
        - Komponentai
        - Interfeisai
        - Resursai
        - Diegimas

Kiekvienas reikalavimui būtina nurodyti statusą (svarbus, ar ne) ir kritiškumą.


2 pagrindiniai dalykai: skaityklė ir šaltinių tikrintuvas

šaltinių tikrintuvas:

- skaityklė turi vartotojo interfeisą (UI) ir backend'ą

- moduliai kaip ir du reikalingi: naujienų ir vartotojo

- filtravimo modulio

- sukurti/ištrinti/keisti filtrą užduotims

- pagalbos modulio (vedlys, FAQ)

- modulis stebėsiantis šaltinių aktyvumą, kuris siūlys atsisakyti

- naujienų valdymo modulį

- administracijos modulio

- apmokėjimų modulis

- Duomenų bazė

- HTTP serveris


Šaltinių tikrintuvas:

parseris ir apdorotojas

